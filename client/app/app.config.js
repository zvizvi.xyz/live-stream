export default ['$locationProvider', '$stateProvider', '$urlRouterProvider', function ($locationProvider, $stateProvider, $urlRouterProvider) {
  'ngInject';
  // @see: https://github.com/angular-ui/ui-router/wiki/Frequently-Asked-Questions
  // #how-to-configure-your-server-to-work-with-html5mode
  // $locationProvider.html5Mode(true);
  // $locationProvider.html5Mode(true).hashPrefix('!');
  $locationProvider.hashPrefix('');
  $stateProvider
    .state('home', {
      url: '/',
      component: 'home'
    })
    .state('streamShow', {
      url: '/show/:streamName',
      component: 'streamShow'
    });
  $urlRouterProvider.otherwise('/');
}]
