export default function () {
  let vm;

  class HeaderController {
    constructor () {
      vm = this;
      vm.name = 'header';
    }
  }

  return new HeaderController();
}
