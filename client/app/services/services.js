import angular from 'angular';
import googleAnalyticsService from './google-analytics.service';
import streamsService from './streams';

let servicesModule = angular.module('app.services', [
  googleAnalyticsService,
  streamsService
])
  .name;

export default servicesModule;
