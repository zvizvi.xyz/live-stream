export default function ($scope, streamsService, googleAnalyticsService) {
  let vm;

  class HomeController {
    constructor () {
      vm = this;
      vm.name = 'home';
    }

    $onInit () {
      vm.filter = '';
      vm.list = [];
      vm.categoryList = [];

      streamsService.getStreams()
        .then((streamList) => {
          vm.list = streamList;
        });
      streamsService.getStreamCategories()
        .then((categoryList) => {
          vm.categoryList = categoryList;
        });

      googleAnalyticsService.setPage('מצלמות הכותל המערבי בשידור חי');
    }

    setFilter (filter) {
      vm.filter = filter;
    }
  }

  return new HomeController();
}
