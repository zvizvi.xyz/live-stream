import angular from 'angular';
import Home from './home/home';
import StreamShow from './stream-show/stream-show';

let componentModule = angular.module('app.components', [
  Home,
  StreamShow
])
  .name;

export default componentModule;
