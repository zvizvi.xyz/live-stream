import angular from 'angular';
import streamShowComponent from './stream-show.component';

let streamShowModule = angular.module('streamShow', [
])
  .component('streamShow', streamShowComponent)
  .name;

export default streamShowModule;
