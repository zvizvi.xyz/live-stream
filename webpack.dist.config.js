const webpack = require('webpack');
const path = require('path');
const config = require('./webpack.config');

config.output = {
  filename: '[name].[contenthash].bundle.js',
  publicPath: '',
  path: path.resolve(__dirname, 'dist')
};

config.optimization = {
  splitChunks: {
    chunks: 'all'
  },
  runtimeChunk: 'single',
  moduleIds: 'hashed'
};

config.mode = 'production';

module.exports = config;
